﻿// dllmain.cpp : Определяет точку входа для приложения DLL.
#include "framework.h"
#include "MemoryLine.h"



char OrigStr[] = "Merry Christmas!";
char ReplStr[] = "Happy New year!";

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
		ReplaceString(OrigStr, ReplStr);
		break;
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

