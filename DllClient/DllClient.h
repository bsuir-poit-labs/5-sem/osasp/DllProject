#pragma once

#include <iostream>
#include <windows.h>
#include "MemoryLine.h"

void DoStaticImport();
void DoDynamicImport();
void DoInjection();