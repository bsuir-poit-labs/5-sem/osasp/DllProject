﻿#include "DllClient.h"
#include <string.h>
#include <tchar.h>

using namespace std;

#define ORIGINAL "Merry Christmas!"
#define REPLACESTRING "ReplaceString"
LPCTSTR DLL_PATH = L"MemoryLine";

char ORIGINAL_STR[] = ORIGINAL;
char REPLACEMENT_STR[] = "Happy New year!";

typedef void (*PFN_ReplaceString)(char*, char*);

int main()
{
	setlocale(LC_ALL, "Russian");

	const char str[] = ORIGINAL;
	int action;

	cout << "1. Статический импорт;" << endl <<
			"2. Динамический импорт;" << endl <<
			"3. Инъекция библиотеки;" << endl << 
			"0. Выход" << endl <<
			"Выберите действие : ";
	cin >> action;

	switch (action)
	{
	case 1:
		DoStaticImport();
		break;
	case 2:
		DoDynamicImport();
		break;
	case 3:
		DoInjection();
		break;
	default:
		break;
	}

	cout << endl <<
		"Исходная строка: " << ORIGINAL << endl <<
		"Заменённая строка: " << str << endl;
}

void DoStaticImport() {
	ReplaceString(ORIGINAL_STR, REPLACEMENT_STR);
}

void DoDynamicImport() {
	HINSTANCE hdll = LoadLibrary(DLL_PATH);
	if (hdll == NULL)
		return;

	PFN_ReplaceString pReplaceString = (PFN_ReplaceString)GetProcAddress(hdll, REPLACESTRING);
	(pReplaceString)(ORIGINAL_STR, REPLACEMENT_STR);
}

void DoInjection() {
	DWORD pid = GetCurrentProcessId();
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

	if (hProcess == INVALID_HANDLE_VALUE) {
		return;
	}

	void* loc = VirtualAllocEx(hProcess, 0, MAX_PATH, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	
	if (!loc) {
		return;
	}

	WriteProcessMemory(hProcess, loc, DLL_PATH, _tcslen(DLL_PATH) + 1, 0);

	HANDLE hThread = CreateRemoteThread(hProcess, 0, 0, (LPTHREAD_START_ROUTINE)LoadLibrary, loc, 0, 0);

	if (hThread) {
		CloseHandle(hThread);
	}

	if (hProcess) {
		CloseHandle(hProcess);
	}
}
